set -ex

SCRIPT_DIR=`dirname "$0"`

export ARCH="${ARCH:-arm}"
export CROSS_COMPILE="${CROSS_COMPILE:-ccache arm-linux-gnueabi-}"

./scripts/kconfig/merge_config.sh arch/arm/configs/imx_v6_v7_defconfig $SCRIPT_DIR/kernel.config

make -j8 zImage dtbs