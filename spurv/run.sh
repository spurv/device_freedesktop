#!/bin/bash

ROOTDIR=`dirname "$0"`
cd $ROOTDIR

umount aosp
sync
mkdir -p aosp
mount -o loop,rw aosp.img aosp

# Enforce probing order so that:
# imx-drm -> /dev/dri/card0
# etnaviv -> /dev/dri/card1
echo etnaviv > /sys/bus/platform/drivers/etnaviv/unbind
echo display-subsystem > /sys/bus/platform/drivers/imx-drm/unbind
echo display-subsystem > /sys/bus/platform/drivers/imx-drm/bind
echo etnaviv > /sys/bus/platform/drivers/etnaviv/bind

bash aosp/launch-container.sh --width 720
umount aosp/vendor
umount aosp/data
umount aosp
